#!/usr/bin/env fennel
(local home (os.getenv "HOME"))
(local hosts
       {:github "https://github.com/"
        :gitlab "https://gitlab.com/"
        }
       )


;;https://gitlab.com/Erik.Lundstedt/luamark.xplr.git


(local git {})

(local gitrepo {})


(fn gitrepo.urlBuilder [host author name]
  (.. host author "/" name ".git" )
  )


(fn gitrepo.new [repo dir]
  {:repo repo :dir dir }
  )


(local repos
       [
        (gitrepo.new (gitrepo.urlBuilder hosts.gitlab "my-suckless-forks" "nsxiv") (.. home "/" "cProgs/suckless/nsxiv")
         )
        ]
       )



(fn git.clone [args]
    (table.concat ["git" "clone" args.repo args.dir ] " ")

    )



(fn git.pull [args]
  (table.concat ["git" "-C" args.dir "pull" ] " ")
  )


(fn clone []
  (local retval [])
  (each [k repo (pairs repos)]
    (table.insert retval (git.clone repo)))
  retval
  )
(fn pull []
  (local retval [])
  (each [k repo (pairs repos)]
    (table.insert retval (git.pull repo)))
    retval
  )

{
 :clone clone
 :pull pull
 }
